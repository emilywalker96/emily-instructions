# Emily's README

## Intro

A fair chunk of this readme will be about my ADHD (Attention Deficit Hyperactivity Disorder), my treatment plan, how my ADHD affects me and how I self manage.

## Medication

My treatment currently consists of, after a bit of trial and error, a course of medication called Atomoxetine (that's the name for the actual drug, the trading name/brand it's sold under is Strattera- to maybe make this clearer, one famous medication for ADHD is called Methylphenidate but it's sold under the trading name Ritalin- only saying this so if I ever mention Strattera you know I'm still talking about my medication). Unlike most people with ADHD I don't respond to stimulants that well- they just make me feel sort of buzzed, like they do for most people without ADHD (so I can't do the old ADHD trick of feeling really chill after a coffee). So I take Strattera which is a non stimulant medication for it.

The particular side effects of Atomoxetine which have affected me are nausea and vomiting. Usually I only show these side effects when I:

-am on a higher dose of Atomoxetine than I was before

-have skipped it for a day or two, as you can lose your tolerance for the drug very very easily

-have taken my meds without food.


If I'm on a higher dose of Atomoxetine the side effects are kind of unavoidable. They might stick around for a couple of days to a week. I don't actually know how long they take to go away because I tend to let the nausea put me off taking them, and then I make the nausea even worse because skipping days starts the side effects back up (bad Emily!).

In terms of taking them without food, this doesn't happen because I just took them first thing and didn't eat for four hours- If I time my taking them when I've eaten very recently or am just about to eat it's fine, I just sometimes take them when I've let a bit too much time pass either way (haven't properly figured out what the cut off point is).

I have a morning routine which gets around the taking without food and skipping days obstacles, that I put in place with a previous manager. I go to the beach, have my breakfast and take them with a drink. (This time can help with punctuality as it provides a bit of a "buffer" if I'm running late for some reason, so if I really have to I can go straight to work and still be on time, BUT I should never really do this because it throws my routine and then I might not take my meds that day).

This works for lots of reasons- I get a drink for free (I always forget to buy a drink whenever I eat a meal usually so then I have nothing to take them with), I have food obviously and it's actually a filling meal so I'm definitely not going to be sick, and I can usually get soy milk at the beach (if I drink my meds with water or a similarly thin liquid they're harder to swallow and it's awful taking them, which then means I skip days from procrastinating taking them).


## Management Strategies


Getting enough sleep is really important for managing my ADHD for two reasons: when I don't I'm more likely to not take my meds, as I'll set my alarm later and need to go straight to work rather than going to the beach beforehand, and being tired greatly exacerbates my symptoms. I don't really have strategies in place for getting a good night's sleep which is something I want to work on (main problem right now is spending time with partner who goes to sleep 1-3 hours later than I do- maybe book out time to spend together before I go to sleep?).

I find music really really distracting generally (this is why I leave the room when the retro playlist is playing because I find it extremely difficult to think of things to write when it's on). I don't listen to music while doing my work for this reason because my productivity absolutely tanks. I've stopped bringing headphones out of the house to listen to while out and about because I think I'm more prone to leaving my belongings in places I visit and on buses when I do.

Mindfulness really helps, and conversely a lack of it detracts. I find using my phone a lot is probably one of the biggest exacerbators of my symptoms, because I'm not really "being where I am", but it's simultaneously a hard habit to knock. My phone use was brought up as an issue early in my probation, but I got around this my putting my phone in my desk drawer or bag while in work. I've experimented with some different means of controlling my phone use outside of work with mixed results, still trying to figure that particular one out (about to try a very very low data plan).

Setting reminders (that I will realistically see) and sticking to a routine seem to be the best ways to self manage in work. I'm pretty out of sight, out of mind- which works great for things like my phone but not so much things I need to do. An example would be that I formed an agreement with a previous manager to never full screen my applications because I would miss meeting reminders from outlook in the corner of my screen. Sticking to a routine also helped improve my punctuality which was previously found to be a cause for concern, and as described earlier is invaluable for taking my meds.

## Symptoms

Lastly I would probably be remiss if I didn't include the ways my ADHD affects my work. This is pretty difficult for me to do- I sometimes can't tell what's "normal" forgetful behaviour and what's symptomatic. I feel like I don't have an objective view of my symptoms (for instance in the past when I've switched medication I don't feel any difference but everyone else thinks I'm doing much better, and when I come off my meds I often don't feel different either but I soon start getting asked if I'm off them). I will give it a go though.


-I can be pretty averse to planning or thinking about stuff before I do it, although this varies. Some of this is not related to my symptoms- I'm particularly put off planning out sprint work because of my previous experiences at an agency where I felt very rushed all the time and felt there was no time to plan things or test them, only smash out code as fast as possible, which I've got some bad habits from. Sometimes this results in me making mistakes that could have been prevented or not thinking as logically about a problem as I probably could.

-I often feel a need to move (this takes the form of runs and jumps generally but these are obviously not appropriate for the workplace so I try not to) and I'm often quite restless and fidgety, especially when I'm concentrating hard on something (the biggest example is debugging).

-I forget things a lot- this isn't too bad in work. It's generally an issue with things that aren't ticketed work. The main thing I forgot was meetings and I used outlook to help me out with that. I do forget things I've committed to outside of the sprint like PDP and objective time (we are exploring things to help me with this though).
